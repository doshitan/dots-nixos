{ config, pkgs, lib, ... }:

{
  imports =
    [ ./hardware.nix
      ../common/configuration.nix
      ../common/ssd.nix
      ../common/gui.nix
      ../common/gnome3.nix
      ../common/brotherprinter.nix
      ../common/steam-controller.nix
      ../common/teensy.nix
      ../common/yubikey.nix
      ../common/bluetooth.nix
    ];

  boot.kernelPackages = pkgs.linuxPackages_latest;

  networking.hostName = "spike";
  networking.hostId = "3ea55e3c";

  security.apparmor.enable = true;

  services.postgresql = {
    enable = true;
    package = pkgs.postgresql96;
    authentication = "local all all trust";
  };

  # services.tarsnap = {
  #   enable = true;
  #   archives = {
  #     spike = {
  #       directories = [ "/home/doshitan/passwords.kdb" "/home/doshitan/Documents" ];
  #     };
  #   };
  # };

  virtualisation.virtualbox.host.enable = true;
  virtualisation.virtualbox.guest.enable = true;
}
