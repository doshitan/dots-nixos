{ config, lib, pkgs, ... }:

{
  imports =
    [ <nixpkgs/nixos/modules/installer/scan/not-detected.nix>
    ];

  boot.initrd.availableKernelModules = [ "xhci_hcd" "ehci_pci" "ahci" "usb_storage" ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/f2f21b36-1651-4b87-b6ca-dca145779b63";
      # device = "/dev/disk/by-label/nixos";
      fsType = "btrfs";
      options = ["subvol=root" "compress=lzo" "autodefrag"];
    };

  fileSystems."/home" =
    { device = "/dev/disk/by-uuid/f2f21b36-1651-4b87-b6ca-dca145779b63";
      # device = "/dev/disk/by-label/nixos";
      fsType = "btrfs";
      options = ["subvol=home" "compress=lzo" "autodefrag"];
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/F9CD-63FE";
      # device = "/dev/disk/by-label/boot";
      fsType = "vfat";
    };

  swapDevices =
    [ { device = "/dev/disk/by-uuid/e8e133d4-0219-441a-aaca-2269d2796f42";
        # device = "/dev/disk/by-label/swap";
      }
    ];

  nix.maxJobs = 4;

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  hardware = {
    bluetooth.enable = true;

    pulseaudio.enable = true;

    trackpoint.emulateWheel = true;

    opengl.s3tcSupport = true;

    enableAllFirmware = true;
  };

  services.tlp.enable = true;
}
