{ config, pkgs, ... }:

{
  imports = [ ../common/xmonad.nix ];

  networking.useNetworkd = true;
  networking.wireless.enable = true;
  networking.wireless.userControlled.enable = true;
  networking.interfaces = {
    wlp3s0 = { useDHCP = true; };
  };
  networking.wireless.interfaces = [ "wlp3s0" ];
}
