{ config, pkgs, lib, ... }:

let
  mkMadisonNginxServerCfg = serverName: clientDir: ''

    server {
      listen 80;

      server_name ${serverName};

      #root /home/doshitan/projects/open-gov/madison/server/public;

      location / {
        root /home/doshitan/projects/open-gov/madison/${clientDir};
        index index.html;
        try_files $uri $uri/ /index.html;
      }

      location /api/ {
        root /home/doshitan/projects/open-gov/madison/server/public;
        #rewrite ^/api/(.*)$ /$1 break;
        index index.php;
        try_files $uri $uri/ /index.php?$query_string;
      }

      location ~ \.php$ {
        root /home/doshitan/projects/open-gov/madison/server/public;
        #try_files $uri /index.php =404;
        #fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/run/phpfpm/madison;
        fastcgi_index index.php;

        fastcgi_param  QUERY_STRING       $query_string;
        fastcgi_param  REQUEST_METHOD     $request_method;
        fastcgi_param  CONTENT_TYPE       $content_type;
        fastcgi_param  CONTENT_LENGTH     $content_length;
        fastcgi_param  SCRIPT_NAME        $fastcgi_script_name;
        fastcgi_param  REQUEST_URI        $request_uri;
        fastcgi_param  DOCUMENT_URI       $document_uri;
        fastcgi_param  DOCUMENT_ROOT      $document_root;
        fastcgi_param  SERVER_PROTOCOL    $server_protocol;
        fastcgi_param  SCRIPT_FILENAME    $document_root$fastcgi_script_name;
        fastcgi_param  PATH_INFO          $fastcgi_script_name;
        fastcgi_param  GATEWAY_INTERFACE  CGI/1.1;
        fastcgi_param  SERVER_SOFTWARE    nginx/$nginx_version;
        fastcgi_param  REMOTE_ADDR        $remote_addr;
        fastcgi_param  REMOTE_PORT        $remote_port;
        fastcgi_param  SERVER_ADDR        $server_addr;
        fastcgi_param  SERVER_PORT        $server_port;
        fastcgi_param  SERVER_NAME        $server_name;
        fastcgi_param  REDIRECT_STATUS    200;
      }
    }
  '';
  madisonNginxCfg = lib.concatStrings [
    (mkMadisonNginxServerCfg "mymadison.doshitan" "client/app")
    (mkMadisonNginxServerCfg "mymadison-prod.doshitan" "client/build")
  ];
in {
  imports =
    [ ./hardware.nix
      ../common/configuration.nix
      ../common/ssd.nix
      ../common/gui.nix
      ../common/gnome3.nix
      ../common/brotherprinter.nix
    ];

  networking.hostName = "edward";
  networking.hostId = "92d08343";
  networking.extraHosts = ''
    127.0.0.1 mymadison.doshitan mymadison-prod.doshitan mymadison-elm.doshitan
    192.168.122.127 phabricator.doshitan
  '';

  programs.adb.enable = true;

  security.apparmor.enable = true;

  # services.gnunet.enable = true;

  services.tarsnap = {
    enable = true;
    archives = {
      edward = {
        directories = [ "/home/doshitan/passwords.kdb" "/home/doshitan/Documents" ];
      };
    };
  };

  services.postgresql = {
    enable = true;
    package = pkgs.postgresql94;
    authentication = "local all all trust";
  };

  services.mysql = {
    enable = true;
    # replication.role = "master";
    package = pkgs.mysql;
  };

  services.phpfpm = {
    phpOptions = ''
      cgi.fix_pathinfo=0
    '';
    poolConfigs = {
      madison = ''
        listen = /run/phpfpm/madison
        listen.owner = doshitan
        user = doshitan
        pm = dynamic
        pm.max_children = 75
        pm.start_servers = 10
        pm.min_spare_servers = 5
        pm.max_spare_servers = 20
        pm.max_requests = 500
      '';
    };
  };

  services.nginx.enable = true;
  services.nginx.user = "doshitan";
  services.nginx.httpConfig = madisonNginxCfg;

  services.udev.extraRules = ''
    SUBSYSTEM=="usb", ATTRS{idVendor}=="1ffb", ATTRS{idProduct}=="0101", ENV{ID_MM_DEVICE_IGNORE}="1"
    SUBSYSTEM=="usb", ATTRS{idVendor}=="1ffb", ATTRS{idProduct}=="2300", ENV{ID_MM_DEVICE_IGNORE}="1"
  '';

  environment.systemPackages = with pkgs; [
    pkgs.steamcontroller-udev-rules
  ];

  nixpkgs.config.packageOverrides = pkgs: {
    steamcontroller-udev-rules = pkgs.writeTextFile {
      name = "steamcontroller-udev-rules";
      text = ''
        # This rule is needed for basic functionality of the controller in
        # Steam and keyboard/mouse emulation
        SUBSYSTEM=="usb", ATTRS{idVendor}=="28de", MODE="0666"

        # This rule is necessary for gamepad emulation; make sure you
        # replace 'pgriffais' with the username of the user that runs Steam
        KERNEL=="uinput", MODE="0660", GROUP="wheel", OPTIONS+="static_node=uinput"
        # systemd option not yet tested
        #KERNEL=="uinput", SUBSYSTEM=="misc", TAG+="uaccess", TAG+="udev-acl"

        # HTC Vive HID Sensor naming and permissioning
        KERNEL=="hidraw*", SUBSYSTEM=="hidraw", ATTRS{idVendor}=="0bb4", ATTRS{idProduct}=="2c87", MODE="0666"
      '';
      destination = "/etc/udev/rules.d/99-zz-steamcontroller.rules";
    };
  };

  services.udev.packages = [ pkgs.steamcontroller-udev-rules ];


  #virtualisation.virtualbox.host.enable = true;
  #virtualisation.virtualbox.guest.enable = true;

  virtualisation.libvirtd.enable = true;
  networking.firewall.checkReversePath = false;

  users.extraUsers.doshitan = {
    extraGroups = [ "libvirtd" "adbusers" ];
  };
}
