#! /usr/bin/env sh

if [ ! -z "$HOST" ]; then
  HOSTNAME="$HOST"
elif [ ! -z $(hostname) ]; then
  HOSTNAME=$(hostname)
else
  echo "Could not determine hostname"
  exit 1
fi

if [ -e "$HOSTNAME/configuration.nix" ]; then
  if [ -e configuration.nix ]; then
    read -e -p "configuration.nix already exists, overwrite? (y/n) " -i "y" -n 1 y_or_n && [ "$y_or_n" == "n" ] && exit
  fi
  ln -srf "$HOSTNAME/configuration.nix" configuration.nix
fi
