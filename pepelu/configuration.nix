{ config, pkgs, ... }:

{
  imports =
    [ ./hardware-configuration.nix
      ../common/configuration.nix
      ../common/gui.nix
      ../common/gnome3.nix
      # ../common/brotherprinter.nix
      ../common/yubikey.nix
      ../common/bluetooth.nix
    ];

  boot.kernelPackages = pkgs.linuxPackages_latest;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "pepelu"; # Define your hostname.

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "18.09"; # Did you read the comment?

  services.syncthing = {
    enable = true;
    user = "doshitan";
    dataDir = "/home/doshitan";
    configDir = "/home/doshitan/.config/syncthing";
    openDefaultPorts = true;
  };

  hardware = {
    cpu.intel.updateMicrocode = true;
  };

  programs.sedutil.enable = true;

  # services.tlp.enable = true;

  services.fwupd.enable = true;
}
