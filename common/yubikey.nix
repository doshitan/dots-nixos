{ config, pkgs, ... }:

{
  hardware.u2f.enable = true;
  services.udev.packages = with pkgs; [
    yubikey-personalization
  ];
  services.pcscd.enable = true;

  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };
}
