{ config, pkgs, ... }:

{
  boot.tmpOnTmpfs = true;

  hardware = {
    # patents have expired, just waiting on updated Mesa that has it integrated
    # by default to hit NixOS
    opengl.s3tcSupport = true;

    # non-free but fully working is preferable most of the time
    enableRedistributableFirmware = true;
  };

  networking.extraHosts = ''
    # https://github.com/syl20bnr/spacemacs/issues/3422
    127.0.0.1 host.does.not.exist
  '';

  # a necessary or convenient evil sometimes
  nixpkgs.config.allowUnfree = true;

  nix.binaryCaches = [
    "https://cache.nixos.org/"
    "https://nixcache.reflex-frp.org"
  ];

  nix.trustedBinaryCaches = config.nix.binaryCaches;

  nix.binaryCachePublicKeys = [
   "ryantrinkle.com-1:JJiAKaRv9mWgpVAz8dwewnZe0AzzEAzPkagE9SP5NWI="
  ];

  environment.systemPackages = with pkgs; [
    htop
    vim_configurable
  ];

  # building/including ruby is huge bloat
  nixpkgs.config.vim.ruby = false;

  # setup ZSH as a proper NixOS interactive shell, so the system-wide config
  # picks up important things
  programs.zsh.enable = true;

  fonts = {
    fontconfig.enable = true;
    fontconfig.defaultFonts.monospace = [ "Fira Mono" ];
    enableFontDir = true;
    enableGhostscriptFonts = true;
    fonts = with pkgs; [
      fira
      fira-mono
      powerline-fonts
      corefonts
      noto-fonts-emoji
      emacs-all-the-icons-fonts
    ];
    # fontconfig.ultimate.enable = false;
    # fontconfig.ultimate.substitutions = "combi";
  };

  # generally just want a ntp client, so use timesyncd by default
  services.timesyncd.enable = true;
  time.timeZone = "America/Chicago";

  users.mutableUsers = false;

  # generate password file contents with something like:
  # openssl passwd -1 <pass>
  users.extraUsers.doshitan = {
    isNormalUser = true;
    extraGroups = [ "audio" "systemd-journal" "wheel" ];
    passwordFile = "/etc/nixos/${config.networking.hostName}/doshitan.password";
    shell = "${pkgs.zsh}/bin/zsh";
  };
}
