{ config, pkgs, ... }:

{
  # TODO: figure out wireless printing

  # TODO: may be needed for discovering wirelessly?
  # services.avahi.enable = true;

  services.printing = {
    enable = true;
    drivers = [ pkgs.gutenprint pkgs.brgenml1cupswrapper pkgs.brgenml1lpr ];
  };
}
