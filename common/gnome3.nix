{ config, pkgs, ... }:

{
  services.xserver = {
    desktopManager.gnome3.enable = true;
    # desktopManager.default = "gnome3";
    displayManager.gdm = {
      enable = true;
    };
  };

  services.gnome3.chrome-gnome-shell.enable = true;
}
