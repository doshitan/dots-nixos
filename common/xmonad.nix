{ config, pkgs, ... }:

{
  services.xserver = {
    windowManager.xmonad.enable = true;
    windowManager.xmonad.enableContribAndExtras = true;
    windowManager.default = "xmonad";
    desktopManager.default = "none";

    displayManager.lightdm = {
      enable = true;
      extraSeatDefaults = ''
        greeter-show-manual-login=true
        greeter-hide-users=true
        allow-guest=false
      '';
    };

    displayManager.desktopManagerHandlesLidAndPower = false;
  };
}
