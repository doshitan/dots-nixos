{ config, pkgs, lib, ... }:

{
  hardware = {
    bluetooth.enable = true;

    pulseaudio.enable = true;

    # headset support
    pulseaudio.package = pkgs.pulseaudioFull;
    pulseaudio.extraModules = [ pkgs.pulseaudio-modules-bt ];
  };
}
