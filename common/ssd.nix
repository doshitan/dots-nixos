{ config, pkgs, ... }:

{
  # TODO should only do this if the swap partition is on an SSD, so make it configurable
  boot.kernel.sysctl = { "vm.swappiness" = 1; };

  # TODO parameterize on block and scheduler name
  systemd.services.set-io-scheduler = {
    description = "Configure I/O Scheduler";
    wantedBy = [ "multi-user.target" ];
    unitConfig.RequiresMountsFor = "/sys/block/sda/queue";
    serviceConfig = {
      Type = "oneshot";
      ExecStart = "${pkgs.stdenv.shell} -c 'echo deadline >/sys/block/sda/queue/scheduler'";
    };
  };
}
